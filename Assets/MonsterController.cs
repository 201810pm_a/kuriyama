﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class MonsterController: MonoBehaviour
{
    //プレイヤーの位置
    public Transform target;

    NavMeshAgent agent;
    Animator animator;

    //当たり判定のオブジェクト
    public GameObject HitjJudgment;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        agent.SetDestination(target.position);
    }

    public void Run()
    {

    }
}
