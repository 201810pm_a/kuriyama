﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(CustomThirdPersonCharacter))]
    public class CustomThirdPersonUserControl : MonoBehaviour
    {
        private CustomThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private BattleCharacter m_BattleCharacter; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.


        private void Start()
        {
            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<CustomThirdPersonCharacter>();
            m_BattleCharacter = GetComponent<BattleCharacter>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }

        private int x = 0;
        private bool _down;
        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            bool crouch = Input.GetKey(KeyCode.C);
            bool back = Input.GetKey("down");


            x++;
            if (Input.GetKey("down") || Input.GetKey(KeyCode.S))
            {
                //Debug.LogError(x + "   1");
                _down = true;
                back = true;
            }
            //else if (v < 0.0f)
            //{
            //    Debug.LogError(x + "    2");
            //    h = 0.0f;
            //    v = 0.0f;
            //}
            //Debug.LogError(x + "h : " + h);
            //Debug.LogError(x + "v : " + v);


            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                m_BattleCharacter.AttackTest();
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Debug.LogError("111111111");
                m_BattleCharacter.Action(1);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Debug.LogError("2222222222");
                m_BattleCharacter.Action(2);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Debug.LogError("3333333333");
                m_BattleCharacter.Action(3);
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                Debug.LogError("4444444444444");
                m_BattleCharacter.Action(4);
            }


            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v * m_CamForward + h * m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v * Vector3.forward + h * Vector3.right;
            }
#if !MOBILE_INPUT
            // walk speed multiplier
            if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

            // pass all parameters to the character control script
            m_Character.Move(m_Move, crouch, m_Jump, back);
            m_Jump = false;
        }
    }
}
