﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

using UnityEngine.EventSystems;
using UnityEngine.UI; // ←※これを忘れずに入れる

public class Enemy : MonoBehaviour {
    Animator _animator;
    NavMeshAgent _navMeshAgent;
    public GameObject target;
    AudioSource _dieSound;
    bool _dieFlag;
    SphereCollider _sphereCollider;
    public EnemyWeapon _weapon;


    private void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.enabled = false;
    }

    // Use this for initialization
    void Start () {
        _navMeshAgent.enabled = true;
        _sphereCollider = GetComponent<SphereCollider>();
        _dieSound = GetComponent<AudioSource>();
        _animator = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
        // NavMeshが準備できているなら
        if (_navMeshAgent.pathStatus != NavMeshPathStatus.PathInvalid && target != null)
        {
            // NavMeshAgentに目的地をセット
            _navMeshAgent.SetDestination(target.transform.position);
        }
    }

    public void Damage(int power)
    {
        _animator.SetTrigger("Damage");
    }


    private void Die()
    {
        if (_dieFlag == false)
        {
            _dieFlag = true;
            _sphereCollider.enabled = false;
            DieEffect();
            _animator.SetBool("Die", true);
            _dieSound.Play();
            Debug.LogError("アニメ");
            StartCoroutine(DestroyObject());
        }
    }

    private IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }

    public virtual void DieEffect()
    {
        Debug.Log("Enemy DieEffect");
    }


    void Hit()
    {
        Debug.LogError("Hitttttttttt");
    }
    void FootL()
    {
        Debug.LogError("FootL");
    }
    void FootR()
    {
        Debug.LogError("FootR");
    }

    void HitStart()
    {
        Debug.LogError("HitStart");
        _weapon.HitPointCollider.enabled = true;
    }

    void HitEnd()
    {
        Debug.LogError("HitEnd");
        _weapon.HitPointCollider.enabled = false;
    }

    void Effect(int num)
    {
        Debug.LogError("num  " + num);
        GameObject effect = Instantiate(
            _weapon.effects[num],
            _weapon.transform.position,
            Quaternion.identity);

    }
}
