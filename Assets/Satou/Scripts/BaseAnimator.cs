﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAnimator : StateMachineBehaviour {

    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetInteger("ActionNum") >= 0 || animator.GetInteger("MultiAction") >= 0)
        {
            animator.SetLayerWeight(animator.GetLayerIndex("Upperbody"), 0);
        }
        else
        {
            animator.SetLayerWeight(animator.GetLayerIndex("Upperbody"), 1);
        }

        animator.SetInteger("ActionNum", -1);
        animator.SetInteger("MultiAction", -1);


    }

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetInteger("MultiAction") >= 0)
        {
            animator.SetInteger("MultiAction", 0);
        }
    }

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {



        //if (animator.GetInteger("ActionNum") < 0 && animator.GetInteger("MultiAction") < 0)
        //{
        //    animator.SetLayerWeight(animator.GetLayerIndex("Upperbody"), 1);
        //}
        //else
        //{
        //    animator.SetLayerWeight(animator.GetLayerIndex("Upperbody"), 0);
        //}
    }

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateMachineEnter is called when entering a statemachine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash){
    //
    //}

    // OnStateMachineExit is called when exiting a statemachine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash) {
    //
    //}
}
