﻿using UnityEngine;
using System.Collections;

public class Test_PopupDamage : MonoBehaviour
{

    /// <summary>
    /// 出力先キャンバス
    /// </summary>
    public GameObject TargetCanvas;

    /// <summary>
    /// ポップアップするテキストオブジェクト
    /// NumberTextScript付き
    /// </summary>
    public GameObject PopupText;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var temp = new GameObject();
            PopupDamageGen gen = temp.AddComponent<PopupDamageGen>();
            gen.PopupString = ((int)(Random.value * 10000.0f)).ToString();
            gen.PopupPosition = Input.mousePosition;
            gen.PopupTextWidth = 20.0f;
            gen.TargetCanvas = this.TargetCanvas;
            gen.PopupTextObject = this.PopupText;
            gen.Popup();
        }
    }
}