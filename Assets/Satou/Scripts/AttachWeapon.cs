﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachWeapon : MonoBehaviour {
    public Transform handle;
    public Weapon weaponPrefab;
    // Use this for initialization
    void Start () {
        if(weaponPrefab != null)
        {
            Attach();
        }  
	}

    private void Attach()
    {
        Weapon weapon = (Weapon)Instantiate(
            weaponPrefab,
            handle.position,
            handle.rotation);

        weapon.transform.parent = handle.transform;
        weapon.transform.localPosition -= weapon.handle.transform.localPosition;
        weapon.transform.localRotation = weapon.handle.transform.localRotation;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
